# Challenge5
Thankfully it's done

# List of API

- Index (GET) = http://localhost:8015/

- All Car from Dashboard (GET) = http://localhost:8015/cars (index-> klik button register-> ada modal-> klik dashboard)

- Create Car (GET)= http://localhost:8015/cars/create 

- Add More Car List (POST) = /cars/create

- Update Car (GET)= http://localhost:8015/cars/:id/update (mendapat update informasi car dari id car yang dipilih di halaman car)

- Form Action for Update a Car from ID (POST) : /cars/id

- List of Small Cars (GET) : http://localhost:8015/cars/small

- List of Medium Cars (GET) : http://localhost:8015/cars/medium

- List of Large Cars (GET) : http://localhost:8015/cars/large

- Delete Car (GET): /cars/delete/:id

# Database and ERD
https://drive.google.com/drive/folders/1sVRnMN6Pdeie5THkLPR8ofaZ857LsWsA?usp=sharing
